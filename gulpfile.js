(function() {
  var browserSync, debug, del, gulp, handleError, pug, minifyCss, htmlmin, paths, plumber, sass, sourcemaps, runSequence;

  gulp = require("gulp");
  minifyCss = require("gulp-minify-css");
  del = require("del");
  pug = require("gulp-pug");
  sass = require("gulp-sass");
  sourcemaps = require("gulp-sourcemaps");
  browserSync = require("browser-sync");
  htmlmin = require("gulp-htmlmin");
  plumber = require("gulp-plumber");
  runSequence = require('run-sequence').use(gulp);

  paths = {
    src: "./src",
    compile: "./tmp",
    build: "./dist",
    npm: "./node_modules"
  };

  debug = true;

  gulp.task("browser-sync", function() {
    return browserSync({
      port: 3000,
      server: {
        baseDir: paths.compile
      }
    });
  });

  handleError = function(err) {
    console.log(">> ERROR <<");
    console.log(err);
    return this.emit('end');
  };

  gulp.task("js", function() {
    return gulp.src([
        paths.src + "/**/*.js"
      ])
      .pipe(plumber(handleError))
      .pipe(gulp.dest(paths.compile));
  });

  gulp.task("pug", function() {
    return gulp.src([
        paths.src + "/**/*.pug"
      ])
      .pipe(plumber(handleError))
      .pipe(pug({
        pretty: true
      }))
      .pipe(gulp.dest(paths.compile));
  });

  gulp.task("sass", function() {
    return gulp.src([
      paths.src + "/**/*.scss"
    ])
    .pipe(plumber(handleError))
    .pipe(sass({
      style: 'expanded'
    }))
    .pipe(sourcemaps.init())
    .pipe(minifyCss())
    .pipe(sourcemaps.write())
    .pipe(gulp.dest(paths.compile));
  });

  gulp.task("css", function() {
    return gulp.src([
      paths.src + "/**/*.css"
    ])
    .pipe(minifyCss())
    .pipe(sourcemaps.write())
    .pipe(gulp.dest(paths.compile));
  });

  gulp.task('copy-assets', function() {
    return gulp.src(paths.src + "/assets/**/*")
      .pipe(gulp.dest(paths.compile + "/assets"));
  });

  gulp.task('compile', function(callback) {
    return runSequence("clean",
                       "pug",
                       "sass",
                       "css",
                       "js",
                       "copy-assets",
                       callback);
  });

  gulp.task("templates", ["pug"], function() {
    return gulp.src([paths.compile + "/**/*.html"])
      .pipe(htmlmin({
        collapseWhitespace: true
      }))
      .pipe(gulp.dest(paths.compile));
  });

  gulp.task("clean", function(cb) {
    return del([paths.build + "/", paths.compile + "/"], cb);
  });

  gulp.task("serve", ["browser-sync"], function() {
    gulp.watch(
      [paths.src + "/**/*.pug"],
      ["templates", browserSync.reload]
    );
    gulp.watch([paths.src + "/**/*.scss", paths.src + "/**/*.css"], ["sass", browserSync.reload]);
    gulp.watch([paths.src + "/**/*.js"], ["js", browserSync.reload]);
  });

  gulp.task("default", ["compile"]);

}).call(this);